use std::sync::Arc;
use std::error;
use hyper::Client;
use url::percent_encoding::{utf8_percent_encode, PATH_SEGMENT_ENCODE_SET};
use serde_json as json;
use hiirc::{Irc, Channel, IrcWrite};
use module::Module;

pub struct CratesIo;

pub fn init() -> CratesIo {
    CratesIo
}

impl Module for CratesIo {
    fn recognizes(&self, command: &str) -> bool {
        command.starts_with("crate ")
    }
    fn execute(&self, command: &str, config: &super::Config, irc: Arc<Irc>, channel: Arc<Channel>) -> Result<(), Box<error::Error>> {
        let tag = "crate ";
        if !self.recognizes(command) {
            return Err("unknown command".into())
        }

        let command = command[tag.len()..].trim();
        let krate = try!(crate_info(command)).krate;
        let output = format!(
            "{name} ({version}) - {description} -> https://crates.io/crates/{urlname} [https://docs.rs/crate/{urlname}]",
            name = krate.name,
            version = krate.max_version,
            description = krate.description,
            urlname = utf8_percent_encode(&krate.name, PATH_SEGMENT_ENCODE_SET).collect::<String>()
        );

        irc.notice(channel.name(), &output);

        Ok(())
    }
}

#[derive(Deserialize,Debug,Clone,PartialEq,Eq)]
pub struct Info {
    #[serde(rename = "crate")]
    krate: Crate,
}

#[derive(Deserialize,Debug,Clone,PartialEq,Eq)]
pub struct Crate {
    id: String,
    name: String,
    description: String,
    max_version: String,
}

pub fn crate_info(name: &str) -> Result<Info, Box<error::Error>> {
    let url = format!(
        "https://crates.io/api/v1/crates/{}",
        utf8_percent_encode(name, PATH_SEGMENT_ENCODE_SET).collect::<String>()
    );
    let client = Client::new();
    let mut res = try!(client.get(&url).send());
    let info: Info = try!(json::from_reader(res));
    Ok(info)
}
