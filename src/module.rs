use std::sync::Arc;
use std::error;
pub use hiirc::{Irc, IrcWrite,Channel};

pub trait Module {
    fn recognizes(&self, command: &str) -> bool;
    fn execute(&self, command: &str, config: &super::Config, irc: Arc<Irc>, channel: Arc<Channel>) -> Result<(), Box<error::Error>>;
}
