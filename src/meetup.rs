use std::str;
use std::io::{self, Read};
use std::sync::Arc;
use std::error;
use hyper::{self, Client};
use hyper::method::Method;
use url::percent_encoding::{utf8_percent_encode, PATH_SEGMENT_ENCODE_SET};
use rustc_serialize::json;
use hiirc::{Channel, Irc, IrcWrite};
use chrono::{Datelike, NaiveDateTime};
use module::Module;

pub struct Meetup;

pub fn init() -> Meetup {
    Meetup
}

impl Module for Meetup {
    fn recognizes(&self, command: &str) -> bool {
        command.starts_with("meetups")
    }
    fn execute(&self, command: &str, config: &super::Config, irc: Arc<Irc>, channel: Arc<Channel>) -> Result<(), Box<error::Error>> {
        if !self.recognizes(command) {
            return Err("unrecognized command".into());
        }

        for group in &config.meetup.groups {
            match upcoming_events(group) {
                Ok(events) => {
                    if let Some(event) = events.get(0) {
                        let time = event.time + event.utc_offset;
                        let time = NaiveDateTime::from_timestamp((time / 1000) as i64, 0);
                        let group = event.group.name.replace(' ', "_").to_lowercase();
                        let link = generate_link(&event);

                        let message = format!("{group}::<{city},{country}>({year:04}-{month:02}-{day:02}) == {event:?} // {link}",
                            group = group,
                            city = event.venue.city,
                            country = event.venue.country.to_uppercase(),
                            event = event.name,
                            year = time.year(),
                            month = time.month(),
                            day = time.day(),
                            link = link,
                        );
                        irc.notice(channel.name(), &message);
                    }
                },
                Err(e) => println!("!!!err: {:#?}", e)
            }
        }

        Ok(())
    }
}

pub fn is_rate_limit_ok(urlname: &str) -> Result<bool, Box<error::Error>> {
    let client = Client::new();
    let resp = try!(client.request(Method::Options, urlname).send());
    let header = try!(resp.headers.get_raw("X-RateLimit-Remaining")
        .and_then(|v| v.get(0))
        .ok_or("X-RateLimit-Remaining header not found"));
    let header = try!(str::from_utf8(header));
    let remaining: i32 = try!(header.parse());
    let threshold = 5;
    if remaining <= threshold {
        println!("!!!warn: meetup rate limit remaining <= {}", threshold);
    }

    Ok(remaining > threshold)
}

pub fn upcoming_events(urlname: &str) -> Result<Vec<Event>, Box<error::Error>> {
    let url = format!(
        "https://api.meetup.com/{urlname}/events?status=upcoming",
        urlname = utf8_percent_encode(urlname, PATH_SEGMENT_ENCODE_SET).collect::<String>()
    );

    if !try!(is_rate_limit_ok(&url)) {
        return Err("meetup rate limit reached".into())
    }

    let client = Client::new();
    let mut resp = try!(client.get(&url).send());

    let mut body = String::new();
    try!(resp.read_to_string(&mut body));

    let events: Vec<Event> = try!(json::decode(&body));

    Ok(events)
}

pub fn generate_link(event: &Event) -> String {
    let url = format!(
        "https://www.meetup.com/{urlname}/events/{id}/",
        urlname = &event.group.urlname,
        id = &event.id,
    );
    url
}

#[derive(Debug, RustcDecodable)]
pub struct Event {
    pub id: String,
    pub name: String,
    pub time: u64,
    pub utc_offset: u64,
    pub group: Group,
    pub venue: Venue,
}

#[derive(Debug, RustcDecodable)]
pub struct Group {
    pub name: String,
    pub urlname: String,
}

#[derive(Debug, RustcDecodable)]
pub struct Venue {
    pub city: String,
    pub country: String,
}
