#![feature(plugin, custom_derive)]
#![plugin(serde_macros)]
extern crate serde_json;
extern crate hiirc;
extern crate toml;
extern crate rustc_serialize;
extern crate hyper;
extern crate url;
extern crate chrono;
extern crate urlshortener;

use std::sync::Arc;
use std::collections::HashSet;
use std::fs::File;
use std::io::{self, Read};
use hiirc::{Irc, Listener, Code, Message, Event, IrcWrite, Channel, ChannelUser};

mod playpen;
mod meetup;
mod cratesio;
mod module;

use module::Module;

fn main() {
    Gooby::from_config("config.toml")
        .expect("config")
        .run()
        .expect("clean shutdown");
}

struct Gooby {
    nick: String,
    config: Config,
}

impl Gooby {
    fn from_config(path: &str) -> io::Result<Gooby> {
        let mut file = try!(File::open(path));
        let mut toml = String::new();
        try!(file.read_to_string(&mut toml));

        let config: Config = try!(toml::decode_str(&toml)
            .ok_or_else(|| io::Error::new(io::ErrorKind::Other, "invalid config.toml")));

        Ok(Gooby {
            nick: config.nick.clone(),
            config: config,
        })
    }

    fn run(self) -> Result<(), hiirc::Error> {
        let nick = self.config.nick.clone();
        let server = self.config.server.clone();
        let pass = self.config.pass.clone();
        let pass = pass.as_ref();

        let mut settings = hiirc::Settings::new(&server, &nick)
            .username(&nick)
            .realname(&nick);
        
        if let Some(pass) = pass {
            settings = settings.password(&pass);
        }

        settings.dispatch(self)
    }
}

impl Listener for Gooby {
    fn welcome(&mut self, irc: Arc<Irc>) {
        for channel in &self.config.channels {
            irc.join(channel, None);
        }
    }

    fn channel_msg(&mut self, irc: Arc<Irc>, channel: Arc<Channel>, sender: Arc<ChannelUser>, mut message: &str) {
        let nick_prefix = format!("{}: ", self.nick);
        let cmd_prefix = "!";

        // Message needs to come from a joined channel
        if !self.config.channels.contains(channel.name()) {
            return;
        }

        // Pass on message without nick prefix
        let is_nick_prefixed = message.starts_with(&nick_prefix);
        if is_nick_prefixed {
            message = message[nick_prefix.len()..].trim_left();
        }

        // Pass on message without cmd prefix
        let is_cmd_prefixed = message.starts_with(cmd_prefix);
        if is_cmd_prefixed {
            message = &message[cmd_prefix.len()..];
        }

        // Recognized only messages which are prefix with either he bots nick or the cmd prefix
        if !is_nick_prefixed && !is_cmd_prefixed {
            return;
        }

        // Initialize modules
        let playpen = playpen::init();
        let modules: &[&Module] = &[
            &cratesio::init(),
            &meetup::init(),
        ];

        // Only recognize as Rust code when the bot is mentioned and no cmd prefix is given
        if is_nick_prefixed && !is_cmd_prefixed {
            if let Err(e) = playpen.execute(message, &self.config, irc.clone(), channel.clone()) {
                println!("!!!err: {:?}", e);
                irc.notice(channel.name(), "an error occured :(");
            }
            return
        }

        for module in modules {
            if module.recognizes(message) {
                if let Err(e) = module.execute(message, &self.config, irc.clone(), channel.clone()) {
                    println!("!!!err: {:?}", e);
                    irc.notice(channel.name(), "an error occured :(");
                }
                return
            }
        }

        irc.notice(channel.name(), "command not recognized :(");
    }

    fn error_msg(&mut self, _irc: Arc<Irc>, code: &Code, err: &Message) {
        println!("!!!err: [{}] {:?}", code, err);
    }
}

#[derive(Debug,RustcDecodable)]
pub struct Config {
    pub server: String,
    pub nick: String,
    pub pass: Option<String>,
    pub channels: HashSet<String>,
    pub meetup: MeetupConfig,
}

#[derive(Debug,RustcDecodable)]
pub struct MeetupConfig {
    groups: HashSet<String>
}
