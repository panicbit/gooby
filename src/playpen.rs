use std::sync::Arc;
use std::error;
use std::io::{self, Read};
use hyper::{self, Client};
use rustc_serialize::json;
use url::form_urlencoded::byte_serialize;
use hiirc::{self, Irc, Channel, IrcWrite};
use module::Module;

pub struct Playpen;

pub fn init() -> Playpen {
    Playpen
}

impl Module for Playpen {
    fn recognizes(&self, command: &str) -> bool {
        true
    }
    fn execute(&self, command: &str, config: &super::Config, irc: Arc<Irc>, channel: Arc<Channel>) -> Result<(), Box<error::Error>> {
        let code = wrap_code(command);

        match run(&code) {
            Ok(Response { program: None, rustc: output }) => {
                let mut lines = output.lines();
                for line in lines.by_ref().take(3) {
                    irc.notice(channel.name(), line);
                }
                if lines.next().is_some() {
                    let snip_msg = match generate_link(&code) {
                        Ok(link) => format!("~~~ {} ~~~", link),
                        Err(e) => {
                            println!("!!!err: {:?}", e);
                            format!("~~~ Could not generate link ~~~")
                        }
                    };
                    irc.notice(channel.name(), &snip_msg);
                }
            },
            Ok(Response { program: Some(output), .. }) => {
                limited_notice(&irc, channel.name(), &output);
            },
            Err(e) => {
                irc.notice(channel.name(), e.description());
            }
        }
        Ok(())
    }
}

pub fn wrap_code(code: &str) -> String {
    format!(r#"
#![allow(dead_code, unused_variables)]

fn show<T: std::fmt::Debug>(e: T) {{ println!("{{:?}}", e) }}

fn main() {{
    show({{
        {}
    }});
}}

"#, code)
}

pub fn generate_link(code: &str) -> io::Result<String> {
    use urlshortener::{UrlShortener, Provider};
    let url = format!(
        "https://play.rust-lang.org/?code={code}",
        code = byte_serialize(code.as_bytes()).collect::<String>()
    );
    let shortener = UrlShortener::new();
    shortener.generate(&*url, Provider::IsGd)
}

pub fn run(code: &str) -> Result<Response, Box<error::Error>> {
    let payload = Payload {
        code: code.to_owned(),
        version: "stable",
        optimize: "0",
        test: false,
        separate_output: true,
        color: false,
        backtrace: "0",
    };
    let payload = try!(json::encode(&payload));

    let client = Client::new();
    let mut response = try!(client
        .post("https://play.rust-lang.org/evaluate.json")
        .body(&payload)
        .send());

    let mut result = String::new();
    try!(response.read_to_string(&mut result));

    let result: Response = try!(json::decode(&result));

    Ok(result)
}

fn limited_notice(irc: &Irc, target: &str, message: &str) -> Result<(), hiirc::Error> {
    let mut lines = message.lines();
    for line in lines.by_ref().take(3) {
        try!(irc.notice(target, line));
    }
    if lines.next().is_some() {
        try!(irc.notice(target, "~~~ snip ~~~"));
    }

    Ok(())
}

#[derive(RustcEncodable)]
struct Payload {
    code: String,
    version: &'static str,
    optimize: &'static str,
    test: bool,
    separate_output: bool,
    color: bool,
    backtrace: &'static str,
}

#[derive(Debug, RustcDecodable)]
pub struct Response {
    pub rustc: String,
    pub program: Option<String>,
}
